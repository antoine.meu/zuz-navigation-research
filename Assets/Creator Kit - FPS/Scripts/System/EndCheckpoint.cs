﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCheckpoint : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        GameSystem.Instance.StopTimer();
        GameSystem.Instance.FinishRun();
        Destroy(gameObject);
    }

    [SerializeField]
    Transform minimapMarker;
    [SerializeField]
    ParticleSystem minimapParticles;

        private Vector3 initialPosition;
    private float timer;
    const float frequency = 5f;
    const float amplitude = 1.5f;
    const float minimumScale = 1f;
    private void Start()
    {
        if (minimapMarker != null)
        initialPosition = minimapMarker.localScale;
        if (minimapParticles != null)
            minimapParticles.Play();
    }
    private void Update()
    {
        if (minimapMarker != null)
        {
            float oscilation = Mathf.Sin(timer * frequency);
            minimapMarker.localScale = Vector3.one * (oscilation * amplitude + minimumScale+amplitude);

        }
        timer += Time.deltaTime;
        if (timer >= 2*Mathf.PI)
        {
            timer -= 2*Mathf.PI;
        }
    }
}
