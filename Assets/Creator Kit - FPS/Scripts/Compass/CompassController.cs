﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompassController : MonoBehaviour
{
    [SerializeField]
    float angleToExitDebug;


    [SerializeField]
    RectTransform rotatingHand;
    [SerializeField]
    RectTransform keyRotatingHAnd;
    [SerializeField]
    public Text distanceNumber;

    Transform character;
    Transform keyTransform;
    Transform endTarget;
    bool keyExistsInScene;

    // Start is called before the first frame update
    void Start()
    {
        var characters = Object.FindObjectsOfType<Controller>();
        var endTargets = Object.FindObjectsOfType<EndCheckpoint>();
        character = characters[0].transform;
        endTarget = endTargets[0].transform;
    }

    // Update is called once per frame
    void Update()
    {

        if (!keyExistsInScene)
        {
            var keysInScene = Object.FindObjectsOfType<Key>();
            if (keysInScene != null)
            {
                keyTransform = keysInScene[0].transform;
                keyExistsInScene = true;
            }
        }
        UpdateDistance();
        UpdateCompassHand();
    }

    void UpdateCompassHand()
    {

        if (character != null && endTarget != null)
        {
            Transform goal;
            if (keyTransform != null)
                goal = keyTransform;
            else
                goal = endTarget;
            Vector3 direction = new Vector3(character.position.x, 0, character.position.z) - new Vector3(goal.position.x, 0, goal.position.z);
            angleToExitDebug = Vector3.SignedAngle(direction, character.forward, Vector3.up);
            if (rotatingHand != null)
                rotatingHand.localEulerAngles = new Vector3(0, 0, angleToExitDebug);
        }


    }

    void UpdateKeyRotatingHand()
    {

    }
    void UpdateDistance()
    {

        Transform goal;
        if (keyTransform != null)
            goal = keyTransform;
        else
            goal = endTarget;
        if (character != null && endTarget != null)
        {
            float distance = Vector3.Magnitude(character.position - goal.position);
            if (distanceNumber != null)
                distanceNumber.text = Mathf.Floor(distance - 3).ToString();
        }
    }
}
