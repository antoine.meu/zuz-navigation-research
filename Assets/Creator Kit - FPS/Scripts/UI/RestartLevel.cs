﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartLevel : MonoBehaviour
{
    [SerializeField]
    public Text endGameText;


    public void OnEnable()
    {
        if (GameSystem.Instance.IsLastLevel())
            endGameText.text = "Submit your scores and proceed";

      

    }
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextLevel()
    {


            UIAudioPlayer.PlayPositive();
            GameSystem.Instance.NextLevel();

     

    }
}
