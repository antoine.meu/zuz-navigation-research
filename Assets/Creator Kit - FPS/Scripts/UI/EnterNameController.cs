﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterNameController : MonoBehaviour
{
    [SerializeField]
    Text userName;

    public GameObject UserInput;

    // Start is called before the first frame update
    public void Start()
    {
        StartCoroutine(waitGameStart());
    }

    private IEnumerator waitGameStart()
    {
        yield return new WaitForEndOfFrame(); // wait a frame to make sure singletons are setup

        if (string.IsNullOrEmpty(GameSystem.Instance.UserName))
        {
            GameSystem.Instance.StopTimer();
            Controller.Instance.DisplayCursor(true);
        }
        else
        {UserInput.SetActive(false);}
    }

    public void SaveNameAndStart()
    {
        GameSystem.Instance.UserName = userName.text;
        PauseMenu.Instance.ReturnToGame();
    }
}
