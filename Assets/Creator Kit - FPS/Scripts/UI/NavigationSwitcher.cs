﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NavigationSwitcher : MonoBehaviour
{
    [SerializeField]
    GameObject Minimap;
    [SerializeField]
    GameObject FullscreenMap;
    [SerializeField]
    GameObject Compass;
    // Use this for initialization
    void Start()
    {
        Minimap?.SetActive(false);
        FullscreenMap?.SetActive(false);
        Compass?.SetActive(false);

        string sceneName = SceneManager.GetActiveScene().name;
        switch (sceneName.Substring(sceneName.Length - 3))
        {
            case "com":
                Compass.SetActive(true);
                break;
            case "min":
                Minimap.SetActive(true);
                break;
            case "ful":
                FullscreenMap.SetActive(true);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
