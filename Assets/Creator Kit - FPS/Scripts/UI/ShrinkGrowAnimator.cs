﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkGrowAnimator : MonoBehaviour
{
    [SerializeField]
    Transform ObjectToAnimate;

    private float timer;
    [SerializeField]
     float frequency = 5f;
    [SerializeField]
     float amplitude = 1.5f;
    [SerializeField]
     float minimumScale = 1f;

    private Vector3 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        if (ObjectToAnimate != null)
            initialPosition = ObjectToAnimate.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        if (ObjectToAnimate != null)
        {
            float oscilation = Mathf.Sin(timer * frequency);
            ObjectToAnimate.localScale = Vector3.one * (oscilation * amplitude + minimumScale + amplitude);

        }
        timer += Time.deltaTime;
        if (timer >= 2 * Mathf.PI)
        {
            timer -= 2 * Mathf.PI;
        }
    }
}
