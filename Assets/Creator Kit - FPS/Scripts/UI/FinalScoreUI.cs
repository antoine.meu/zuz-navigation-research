﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System;
using GameAnalyticsSDK;

public class FinalScoreUI : MonoBehaviour
{
    public static FinalScoreUI Instance { get; private set; }

    public Text TargetDestroyed;
    public Text Penalty;
    public Text TimeSpent;
    public Text FinalTime;
    public Text FinalScore;
    public Text LevelName;

    void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    public void Display()
    {
        gameObject.SetActive(true);

        float time = GameSystem.Instance.RunTime;
        int targetDestroyed = GameSystem.Instance.DestroyedTarget;
        int totalTarget = GameSystem.Instance.TargetCount;
        int missedTarget = totalTarget - targetDestroyed;
        float penaltyAmount = GameSystem.Instance.TargetMissedPenalty * missedTarget;

        TargetDestroyed.text = targetDestroyed + "/" + totalTarget;
        TimeSpent.text = time.ToString("N2") + "s";
        Penalty.text = missedTarget + "*" + GameSystem.Instance.TargetMissedPenalty.ToString("N2") + "s = " + penaltyAmount.ToString("N2") + "s";
        FinalTime.text = (time).ToString("N2");

        FinalScore.text = GameSystem.Instance.Score.ToString("N");



        LevelName.text = SceneManager.GetActiveScene().name;

        string condensedResultsUnity = "CondensedResults" + ", " +
            //AnalyticsSessionInfo.userId + ", " +
            GameSystem.Instance.UserName + ", " +
            LevelName.text + ", " +
            LevelName.text.Substring(0, 6) + ", " +
            LevelName.text.Substring(LevelName.text.Length - 3) + ", " +
            FinalTime.text + ", " +
            DateTime.UtcNow;
        string condensedResultsGA = //"CondensedResults" + "- " +
                                    //AnalyticsSessionInfo.userId + "- " +
            GameSystem.Instance.UserName + "-" +
            LevelName.text + "-" +
            //LevelName.text.Substring(0, 6) + "- " +
            //LevelName.text.Substring(LevelName.text.Length - 3) + "- " +
            FinalTime.text + "-";// +
                                 //DateTime.UtcNow;

        Analytics.CustomEvent("EndgameResults", new Dictionary<string, object>
        {
            {"UserId", AnalyticsSessionInfo.userId },
            {"UserName", GameSystem.Instance.UserName },
            {"FullLevelName", LevelName.text },
            {"LevelName", LevelName.text.Substring(0,6)  },
            {"NavigationType", LevelName.text.Substring(LevelName.text.Length - 3) },
            {"FinalTime", FinalTime.text },
            {"DateAndTime", DateTime.UtcNow }
        }); // Added for Analytics

        Analytics.CustomEvent(condensedResultsUnity
            );
        //GameAnalytics.Initialize();
        //GameAnalytics.NewDesignEvent(condensedResultsGA);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, GameSystem.Instance.UserName, LevelName.text, FinalTime.text, 0);

    }
}
