﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FullscreenMap : MonoBehaviour
{
    public static FullscreenMap Instance { get; private set; }

    public RawImage MapImageTarget;
    public RectTransform Arrow;

    public int Resolution;

    public MinimapSystem.MinimapSystemSetting MinimapSystemSettings;
    
    public RenderTexture RenderTexture => m_RT;
    
    RenderTexture m_RT;
    float m_Ratio;

    bool m_HeightWasInited;
    float m_InitialHeight;
    Transform mapCenter;    

    void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
        m_Ratio = MapImageTarget.rectTransform.rect.height / MapImageTarget.rectTransform.rect.width;
        m_RT = new RenderTexture(Resolution, Mathf.FloorToInt(Resolution * m_Ratio), 16, RenderTextureFormat.ARGB32);
        MapImageTarget.texture = m_RT;
        m_HeightWasInited = false;

    }

    private void Start()
    {
        var _mapCenter = Object.FindObjectsOfType<MapCenter>();
        mapCenter = _mapCenter[0].transform;

    }



    public void UpdateForPlayerTransform(Transform playerTransform)
    {
        if (!m_HeightWasInited)
        {
            m_HeightWasInited = true;
            m_InitialHeight = playerTransform.position.y;
        }

        Vector3 usedPosition = playerTransform.position;
        float heightDifference = m_InitialHeight - usedPosition.y;
        float heightSign = Mathf.Sign(heightDifference);
        heightDifference = Mathf.FloorToInt(Mathf.Abs(heightDifference / MinimapSystemSettings.heightStep)) * heightSign * heightDifference;

        usedPosition.y = m_InitialHeight + heightDifference;

        if (mapCenter != null)
            MinimapSystem.Render(m_RT, mapCenter.position, playerTransform.forward, MinimapSystemSettings);//MinimapSystem.Render(m_RT, usedPosition, playerTransform.forward, MinimapSystemSettings);

        if (MinimapSystemSettings.isFixed)
        {
            Arrow.rotation = Quaternion.Euler(0, 0, Vector3.SignedAngle(playerTransform.forward, Vector3.forward, Vector3.up));
        }
        else
        {
            Arrow.rotation = Quaternion.identity;
        }
    }
}
